import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View extends JFrame {
	
	private RSA myRSA;
	private JLabel entryLabel;
	private JTextField entryField;
	private JTextArea outputArea;
	private JButton goButton;
	private JScrollPane outputScroller;
	
	
	public View(RSA controller) {
		myRSA = controller;
		
		entryLabel = new JLabel("Texto: ");
		entryField = new JTextField();
		outputArea = new JTextArea("Digite o texto que você deseja criptografar.");
		outputArea.setEnabled(false);
		Color bgColor = UIManager.getColor("TextField.background");
		outputArea.setBackground(bgColor);
		Color fgColor = UIManager.getColor("TextField.foreground");
		outputArea.setDisabledTextColor(fgColor);
		outputArea.setBorder(BorderFactory.createEtchedBorder());
		outputArea.setLineWrap(true);
		outputScroller = new JScrollPane(outputArea);
		goButton = new JButton("Cifrar");
		outputArea.setFont(outputArea.getFont().deriveFont(20f));
		
		this.getContentPane().setLayout(null);

		this.add(entryLabel);
		this.add(entryField);
		this.add(goButton);
		this.add(outputScroller);
		
		outputScroller.setBounds(0,0,640,430);
		goButton.setBounds(550, 428, 80, 20);
		entryField.setBounds(50, 429, 400, 20);
		entryLabel.setBounds(10, 428, 50, 20);
		
		goButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				outputArea.setText(myRSA.toString() +
								"Texto original: " + entryField.getText() + "\n"
								+ "String descriptografada: " + new String(myRSA.decrypt(myRSA.encrypt(entryField.getText().getBytes()))) + "\n"
								);
			}
		});
		
		this.setSize(new Dimension(645,477));
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setTitle("Criptografia/Descriptografia em RSA");
		
		this.setVisible(true);
	}
	
	
}
